package orm.lev.zen_training_view;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/**
 * Created by Lev on 09.01.2016.
 */
public class CustomDrawable extends Drawable {
    private Path mPath;
    private Paint mPaint;

    public CustomDrawable() {
        mPaint = new Paint();
        mPaint.setColor(Color.GRAY);
        mPath = new Path();
    }
    @Override
    public void draw(Canvas canvas) {
        canvas.drawPath(mPath, mPaint);
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        int width = bounds.width();
        int height = bounds.height();

        mPath.moveTo(0.0f, 0.0f);
        mPath.lineTo(width, 0);
        mPath.lineTo(width/2, height);
        mPath.lineTo(0.0f, 0.0f);
        mPath.close();
    }

    @Override
    public int getOpacity() {
        return 0;
    }
}
