package orm.lev.zen_training_view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;


/**
 * TODO: document your custom view class.
 */
public class LevelBar extends View {

    private String mParamTitle;
    private float mMaxLevel;
    private float mMinLevel;
    private float mCurrentLevel;

    private TextPaint mTitlePaint;
    private float mTitleWidth;
    private float mTitleHeight;

    private Paint mBarPaint;
    private float mBarWidth;
    private float mBarHeight;
    private Paint mLevelPaint;

    public LevelBar(Context context) {
        super(context);
        init(null);
    }

    public LevelBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.LevelBar, 0, 0);
        mParamTitle = a.getString(R.styleable.LevelBar_paramTitle);
        mMaxLevel = a.getFloat(R.styleable.LevelBar_maxLevel, 0.0f);
        mMinLevel = a.getFloat(R.styleable.LevelBar_minLevel, 0.0f);
        mCurrentLevel = a.getFloat(R.styleable.LevelBar_currentLevel, 0.0f);
        a.recycle();

        mTitlePaint = new TextPaint();
        mTitlePaint.setColor(Color.BLUE);
        mTitlePaint.setTextSize(20.0f);

        mBarPaint = new Paint();
        mBarPaint.setColor(Color.DKGRAY);

        mLevelPaint = new Paint();
        mLevelPaint.setColor(Color.GRAY);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mTitleWidth = mTitlePaint.measureText(mParamTitle);
        mTitleHeight = mTitlePaint.getFontMetrics().bottom;
        Log.i("DEV", "OnMeasure");
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        Log.i("DEV", "OnLayout");
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // allocations per draw cycle.
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

        // Draw the text.
        canvas.drawText(mParamTitle,
                paddingLeft + (contentWidth - mTitleWidth) / 2,
                paddingTop + mTitleHeight + 20,
                mTitlePaint);

        // Draw Level Bar
        mBarWidth = contentWidth - 20;
        mBarHeight = 40;
        int initPointLeft = paddingLeft + 10;
        int initPointTop = paddingTop + (int) mTitleHeight + 70;
        Rect levelBar = new Rect(initPointLeft, initPointTop, initPointLeft + (int)mBarWidth,
                initPointTop + (int)mBarHeight);
        canvas.drawRect(levelBar, mBarPaint);

        float levelInPrc = this.getLevelInPrc();
        Rect levelBarIndicator = new Rect(initPointLeft, initPointTop,
                initPointLeft + (int)(mBarWidth*levelInPrc),
                initPointTop + (int)mBarHeight);
        canvas.drawRect(levelBarIndicator, mLevelPaint);

        drawLevelTitle(canvas,
                initPointTop - 20,
                initPointLeft,
                initPointLeft+(int)mBarWidth,
                mCurrentLevel, levelInPrc);

    }

    private float getLevelInPrc() {
        return mCurrentLevel/(mMaxLevel-mMinLevel);
    }

    public void setMaxLevel(float maxLevel) {
        mMaxLevel = maxLevel;
        invalidate();
    }

    public void setMinLevel(float minLevel) {
        mMinLevel = minLevel;
        invalidate();
    }

    public void setCurrentLevel(float curLevel) {
        mCurrentLevel = curLevel;
        invalidate();
    }

    private void drawLevelTitle(Canvas canvas, float topBoarder, float leftBoarder,
            float rightBoarder, float level, float levelInPrc) {
        String levelString = String.valueOf(level);
        float textWidth = mTitlePaint.measureText(levelString);
        float textHeight = mTitlePaint.getFontMetrics().bottom;
        float freeSpace = rightBoarder - leftBoarder;
        float x = freeSpace * levelInPrc - textWidth/2;
        float y = topBoarder;
        if((x-textWidth)<leftBoarder) { x = leftBoarder + textWidth/2; }
        canvas.drawText(String.valueOf(level), x, y, mTitlePaint);
    }
}
